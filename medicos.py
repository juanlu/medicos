class Persona:
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni):
        self.nombre = nombre
        self.apellidos = apellidos
        self.fecha_nacimiento = fecha_nacimiento
        self.dni = dni

    def __str__(self):
        return f"Nombre: {self.nombre}, Apellidos: {self.apellidos}, Fecha de Nacimiento: {self.fecha_nacimiento}, DNI: {self.dni}"

    # Getters
    
    def nombre(self):
        return self._nombre

    
    def apellidos(self):
        return self._apellidos

    
    def fecha_nacimiento(self):
        return self._fecha_nacimiento

    
    def dni(self):
        return self._dni

    # Setters
    
    def nombre(self, valor):
        self._nombre = valor

    
    def apellidos(self, valor):
        self._apellidos = valor

    
    def fecha_nacimiento(self, valor):
        self._fecha_nacimiento = valor

    
    def dni(self, valor):
        self._dni = valor

class Paciente(Persona):
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni, historial_clinico):
        super().__init__(nombre, apellidos, fecha_nacimiento, dni)
        self.historial_clinico = historial_clinico
    def __str__(self):
        return f"Nombre: {self.nombre}, Apellidos: {self.apellidos}, Fecha de Nacimiento: {self.fecha_nacimiento}, DNI: {self.dni}, HISTORIAL_CLINICO:{self.historial_clinico}"

    def ver_historial_clinico(self):
        return self.historial_clinico

class Medico(Persona):
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni, especialidad, citas):
        super().__init__(nombre, apellidos, fecha_nacimiento, dni)
        self.especialidad = especialidad
        self.citas = citas
    def __str__(self):
        return f"Nombre: {self.nombre}, Apellidos: {self.apellidos}, Fecha de Nacimiento: {self.fecha_nacimiento}, DNI: {self.dni}, Especialidad: {self.especialidad}, citas: {self.citas}"

    def consultar_agenda(self):
        return self.citas
per1=Persona('juan','pareja','13_10_2002','53815934M')
per2 = Persona("Juan", "Pérez", "1990-01-01", "12345678A")
pac = Paciente("Ana", "Lopez", "1985-05-15", "23456789B", "Historial clínico de Ana")
med = Medico("Carlos", "Martínez", "1975-03-22", "34567890C", "Cardiología", ["Cita 1", "Cita 2"])
print(per1)
print(per2)
print(pac)
print(med)
