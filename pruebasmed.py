import unittest
from medicos import Persona, Paciente, Medico 
class TestPersona(unittest.TestCase):

    def setUp(self):
        # Datos de prueba comunes para usar en las pruebas
        self.persona = Persona("Juan", "Pérez", "13-10-2002", "12345678A")
        self.paciente = Paciente("Betriz", "Lopez", "14-04-1971", "23456789B", "Historial clínico de Ana")
        self.medico = Medico("Carl", "Martínez", "23-10-1968", "34567890C", "Cardiología", ["Cita 1", "Cita 2"])

    # Pruebas para Persona
    def test_persona_str(self):
        expected_str = "Nombre: Juan, Apellidos: Pérez, Fecha de Nacimiento: 13-10-2002, DNI: 12345678A"
        self.assertEqual(str(self.persona), expected_str)

    def test_set_nombre_persona(self):
        self.persona.nombre = "Roberto"
        self.assertEqual(self.persona.nombre, "Roberto")

    def test_set_apellidos_persona(self):
        self.persona.apellidos = "González"
        self.assertEqual(self.persona.apellidos, "González")

    # Pruebas para Paciente
    def test_paciente_historial_clinico(self):
        self.assertEqual(self.paciente.ver_historial_clinico(), "Historial clínico de Ana")

    def test_paciente_herencia(self):
        self.assertTrue(isinstance(self.paciente, Persona))

    # Pruebas para Médico
    def test_medico_especialidad(self):
        self.assertEqual(self.medico.especialidad, "Cardiología")

    def test_medico_consultar_agenda(self):
        self.assertEqual(len(self.medico.consultar_agenda()), 2)
        self.assertIn("Cita 1", self.medico.consultar_agenda())

    # Pruebas adicionales para validación y herencia
    def test_persona_fecha_nacimiento(self):
        self.persona.fecha_nacimiento = "2000-01-01"
        self.assertEqual(self.persona.fecha_nacimiento, "2000-01-01")

    def test_medico_herencia(self):
        self.assertTrue(isinstance(self.medico, Persona))

    def test_medico_set_especialidad(self):
        self.medico.especialidad = "Neurología"
        self.assertEqual(self.medico.especialidad, "Neurología")

if __name__ == '__main__':
    unittest.main()